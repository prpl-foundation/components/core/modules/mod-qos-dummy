/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_QOS_DUMMY_H__)
#define __MOD_QOS_DUMMY_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @defgroup mod-qos-dummy Dummy QoS Module API
 *
 * @brief
 * This module defines the API functions as defined by tr181-qos to implement
 * the vendor specific QoS functionality.
 */

#define MOD_QOS_DUMMY_ZONE "mod-dummy"

/**
 * @ingroup mod-qos-dummy
 * @brief
 *
 * API function called by tr181-qos to activate a queue.
 *
 * @attention
 * A queue must be connected to a scheduler to determine the scheduler
 * identifier. The index of the scheduler in Device.QoS.Scheduler.{i}
 * determines the scheduler ID.
 *
 * @param[in] function_name Equals this function name. Further unused.
 * @param[in] var_node_path Variant of type cstring_t containing the protected
 * Device.QoS.Node.{i} path.
 * @param[out] ret Variant to store data to return to tr181-qos after this API
 * function is finished. Unused here.
 */
int activate_queue(const char* const function_name, amxc_var_t* var_node_path,
                   amxc_var_t* ret);

/**
 * @ingroup mod-qos-dummy
 * @brief
 *
 * API function called by tr181-qos to deactivate a queue.
 *
 * @attention
 * A queue must be connected to a scheduler to determine the scheduler
 * identifier. The index of the scheduler in Device.QoS.Scheduler.{i}
 * determines the scheduler ID.
 *
 * @param[in] function_name Equals this function name. Further unused.
 * @param[in] var_node_path Variant of type cstring_t containing the protected
 * Device.QoS.Node.{i} path.
 * @param[out] ret Variant to store data to return to tr181-qos after this API
 * function is finished. Unused here.
 */
int deactivate_queue(const char* const function_name, amxc_var_t*
                     var_node_path, amxc_var_t* ret);

/**
 * @ingroup mod-qos-dummy
 * @brief
 *
 * API function called by tr181-qos to activate a scheduler.
 *
 * @attention
 * A queue must be connected to a scheduler to determine the scheduler
 * identifier. The index of the scheduler in Device.QoS.Scheduler.{i}
 * determines the scheduler ID.
 *
 * @param[in] function_name Equals this function name. Further unused.
 * @param[in] var_node_path Variant of type cstring_t containing the protected
 * Device.QoS.Node.{i} path.
 * @param[out] ret Variant to store data to return to tr181-qos after this API
 * function is finished. Unused here.
 */
int activate_scheduler(const char* const function_name, amxc_var_t*
                       var_node_path, amxc_var_t* ret);

/**
 * @ingroup mod-qos-dummy
 * @brief
 *
 * API function called by tr181-qos to deactivate a scheduler.
 *
 * @param[in] function_name Equals this function name. Further unused.
 * @param[in] var_node_path Variant of type cstring_t containing the protected
 * Device.QoS.Node.{i} path.
 * @param[out] ret Variant to store data to return to tr181-qos after this API
 * function is finished. Unused here.
 */
int deactivate_scheduler(const char* const function_name, amxc_var_t*
                         var_node_path, amxc_var_t* ret);

/**
 * @ingroup mod-qos-dummy
 * @brief
 *
 * API function called by tr181-qos to activate a shaper.
 *
 * @param[in] function_name Equals this function name. Further unused.
 * @param[in] var_node_path Variant of type cstring_t containing the protected
 * Device.QoS.Node.{i} path.
 * @param[out] ret Variant to store data to return to tr181-qos after this API
 * function is finished. Unused here.
 *
 * @returns -1 until supported.
 *
 * @attention
 * Separate shapers don't exist in Dummy. To shape traffic, use the queue's
 * or scheduler's shaping capabilities.
 */
int activate_shaper(const char* const function_name, amxc_var_t* var_node_path,
                    amxc_var_t* ret);

/**
 * @ingroup mod-qos-dummy
 * @brief
 *
 * API function called by tr181-qos to deactivate a shaper.
 *
 * @param[in] function_name Equals this function name. Further unused.
 * @param[in] var_node_path Variant of type cstring_t containing the protected
 * Device.QoS.Node.{i} path.
 * @param[out] ret Variant to store data to return to tr181-qos after this API
 * function is finished. Unused here.
 *
 * @returns -1 until supported.
 *
 * @attention
 * Separate shapers don't exist in Dummy. To shape traffic, use the queue's
 * or scheduler's shaping capabilities.
 */
int deactivate_shaper(const char* const function_name, amxc_var_t*
                      var_node_path, amxc_var_t* ret);

/**
 * @ingroup mod-qos-dummy
 * @brief
 *
 * API function called by tr181-qos to query interface statistics.
 *
 * @param[in] function_name Equals this function name. Further unused.
 * @param[in] var_node_path Variant of type cstring_t containing the protected
 * Device.QoS.Node.{i} path.
 * @param[out] ret Variant to store statistics to return to tr181-qos after
 * this API function is finished.
 */
int retrieve_stats(const char* const function_name, amxc_var_t* var_node_path,
                   amxc_var_t* ret);

/**
 * @ingroup mod-qos-dummy
 * @brief
 *
 * API function called by tr181-qos to query queue statistics.
 *
 * @param[in] function_name Equals this function name. Further unused.
 * @param[in] var_node_path Variant of type cstring_t containing the protected
 * Device.QoS.Node.{i} path.
 * @param[out] ret Variant to store queue statistics to return to tr181-qos
 * after this API function is finished.
 */
int retrieve_queue_stats(const char* const function_name, amxc_var_t*
                         var_node_path, amxc_var_t* ret);



#ifdef __cplusplus
}
#endif

#endif // __MOD_QOS_DUMMY_H__
