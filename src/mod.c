/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <strings.h>

#include <debug/sahtrace.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>

#ifdef UNIT_TEST
#include "mod.h"
#endif
#include <qosmodule/api.h>
#include "mod-qos-dummy.h"

/** Controller name as defined by tr181-qos. */
#define MOD_QOS_CTRL "qos-ctrl"

/** Module used by libamxm. */
static amxm_module_t* module = NULL;

/**
 * @ingroup mod-qos-dummy
 * @brief
 * Constructor function when the module is loaded by tr181-qos.
 *
 * @returns 0 on success, otherwise -1.
 */
#ifndef UNIT_TEST
static AMXM_CONSTRUCTOR
#else
int
#endif
mod_start(void) {
    int retval = -1;

    //Not all functions have to be implemented. Set them to NULL in that case.
    qos_module_api_funcs_t funcs = {
        .activate_shaper = activate_shaper,
        .activate_scheduler = activate_scheduler,
        .activate_queue = activate_queue,
        .deactivate_shaper = deactivate_shaper,
        .deactivate_scheduler = deactivate_scheduler,
        .deactivate_queue = deactivate_queue,
        .retrieve_statistics = retrieve_stats,
        .retrieve_queue_statistics = retrieve_queue_stats
    };

    retval = qos_module_register(&module, &funcs);
    when_failed(retval, exit);

exit:
    return retval;
}

/**
 * @ingroup mod-qos-dummy
 * @brief
 * Destructor function when the module is unloaded by tr181-qos.
 *
 * @returns 0, even when errors occur.
 */
#ifndef UNIT_TEST
static AMXM_DESTRUCTOR
#else
int
#endif
mod_stop(void) {
    int retval = -1;

    //Do something to deinit the module.

    retval = qos_module_deregister(&module);
    if(0 != retval) {
        SAH_TRACEZ_ERROR(MOD_QOS_DUMMY_ZONE, "Failed to deregister module");
    }

    return 0;
}

