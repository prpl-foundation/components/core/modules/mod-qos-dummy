/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdint.h>

#include <debug/sahtrace.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxm/amxm.h>

#include <qosnode/qos-node-api.h>

#include "mod-qos-dummy.h"

/**
 * Get the node path from the variant.
 *
 * @param[in] var The variant containing a cstring_t type with the a
 * Device.QoS.Node.{i} path as value.
 *
 * @returns the Device.QoS.Node.{i} path if present, NULL on error.
 */
static inline const char* get_node_path_from_variant(const amxc_var_t* const var) {
    return amxc_var_constcast(cstring_t, var);
}

static qos_node_t* get_node_from_path(const amxc_var_t* var_node_path) {
    const char* node_path = NULL;
    qos_node_t* node = NULL;

    when_null(var_node_path, exit);
    node_path = get_node_path_from_variant(var_node_path);
    when_str_empty(node_path, exit);

    node = qos_node_get_node(node_path);
exit:
    return node;
}

int activate_queue(UNUSED const char* const function_name, amxc_var_t*
                   var_node_path, UNUSED amxc_var_t* ret) {
    int retval = -1;
    qos_node_t* node = NULL;

    node = get_node_from_path(var_node_path);
    when_null(node, exit);
    when_true(QOS_NODE_TYPE_QUEUE != node->type, exit);

    //Do something to activate the queue.

    retval = 0;

exit:
    return retval;
}

int deactivate_queue(UNUSED const char* const function_name, amxc_var_t*
                     var_node_path, UNUSED amxc_var_t* ret) {
    int retval = -1;
    qos_node_t* node = NULL;

    node = get_node_from_path(var_node_path);
    when_null(node, exit);
    when_true(QOS_NODE_TYPE_QUEUE != node->type, exit);

    //Do something to deactivate the queue.

    retval = 0;
exit:
    return retval;
}

int activate_scheduler(UNUSED const char* const function_name, amxc_var_t*
                       var_node_path, UNUSED amxc_var_t* ret) {

    int retval = -1;
    qos_node_t* node = NULL;

    node = get_node_from_path(var_node_path);
    when_null(node, exit);
    when_true(QOS_NODE_TYPE_SCHEDULER != node->type, exit);

    //Do something to activate the scheduler.

    retval = 0;
exit:
    return retval;
}

int deactivate_scheduler(UNUSED const char* const function_name, amxc_var_t*
                         var_node_path, UNUSED amxc_var_t* ret) {

    int retval = -1;
    qos_node_t* node = NULL;

    node = get_node_from_path(var_node_path);
    when_null(node, exit);
    when_true(QOS_NODE_TYPE_SCHEDULER != node->type, exit);

    //Do something to deactivate the scheduler.

    retval = 0;
exit:
    return retval;
}

int activate_shaper(UNUSED const char* const function_name, UNUSED amxc_var_t*
                    var_node_path, UNUSED amxc_var_t* ret) {

    int retval = -1;
    qos_node_t* node = NULL;

    node = get_node_from_path(var_node_path);
    when_null(node, exit);
    when_true(QOS_NODE_TYPE_SHAPER != node->type, exit);

    //Do something to activate the shaper.

    retval = 0;
exit:
    return retval;
}

int deactivate_shaper(UNUSED const char* const function_name, UNUSED
                      amxc_var_t* var_node_path, UNUSED amxc_var_t* ret) {

    int retval = -1;
    qos_node_t* node = NULL;

    node = get_node_from_path(var_node_path);
    when_null(node, exit);
    when_true(QOS_NODE_TYPE_SHAPER != node->type, exit);

    //Do something to deactivate the shaper.

    retval = 0;
exit:
    return retval;
}

int retrieve_queue_stats(UNUSED const char* const function_name, UNUSED amxc_var_t*
                         var_node_path, UNUSED amxc_var_t* ret) {
    int retval = -1;
    qos_node_t* node = NULL;
    uint32_t current_shaping_rate = 0;
    uint32_t current_assured_rate = 0;

    when_null(ret, exit);
    node = get_node_from_path(var_node_path);
    when_null(node, exit);
    when_true(QOS_NODE_TYPE_QUEUE != node->type, exit);

    //Do something to populate the variables current_shaping_rate and current_assured_rate.

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, ret, "CurrentShapingRate", current_shaping_rate);
    amxc_var_add_key(uint32_t, ret, "CurrentAssuredRate", current_assured_rate);

    retval = 0;
exit:
    return retval;
}

int retrieve_stats(UNUSED const char* const function_name,
                   amxc_var_t* var_node_path, amxc_var_t* ret) {
    int retval = -1;
    qos_node_t* node = NULL;
    uint32_t tx_packets = 0;
    uint32_t tx_bytes = 0;
    uint32_t dropped_packets = 0;
    uint32_t dropped_bytes = 0;

    when_null(ret, exit);
    node = get_node_from_path(var_node_path);
    when_null(node, exit);
    when_true(QOS_NODE_TYPE_QUEUE != node->type, exit);

    //Do something to populate the variables tx_packets, tx_bytes,
    //dropped_packets and dropped_bytes.

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, ret, "TxPackets", tx_packets);
    amxc_var_add_key(uint32_t, ret, "TxBytes", tx_bytes);
    amxc_var_add_key(uint32_t, ret, "DroppedPackets", dropped_packets);
    amxc_var_add_key(uint32_t, ret, "DroppedBytes", dropped_bytes);

    retval = 0;

exit:
    return retval;
}

