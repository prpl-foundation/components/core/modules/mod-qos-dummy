/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <cmocka.h>

#include <qosnode/qos-node.h>

#include "test_mod_qos_dummy.h"
#include "libamx_mock.h"
#include "mod-qos-dummy.h"
#include "libqosmodule_mock.h"

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif


int test_mod_qos_dummy_setup(UNUSED void** state) {
    return 0;
}

int test_mod_qos_dummy_teardown(UNUSED void** state) {
    return 0;
}

void test_mod_start(UNUSED void** state) {
    int retval = -1;

    expect_any_always(__wrap_qos_module_register, module);
    expect_any_always(__wrap_qos_module_register, funcs);
    will_return(__wrap_qos_module_register, -1);
    retval = mod_start();
    assert_int_equal(retval, -1);

    will_return(__wrap_qos_module_register, 0);
    retval = mod_start();
    assert_int_equal(retval, 0);
}

void test_mod_stop(UNUSED void** state) {
    int retval = -1;

    expect_any_always(__wrap_qos_module_deregister, module);

    will_return(__wrap_qos_module_deregister, 0);
    retval = mod_stop();
    assert_int_equal(retval, 0);
}

static void variant_set_node_path(amxc_var_t* var, const char* path) {

    assert_int_equal(amxc_var_set_type(var, AMXC_VAR_ID_CSTRING), 0);
    assert_int_equal(amxc_var_set(cstring_t, var, path), 0);
}

void test_activate_queue(UNUSED void** state) {
    int retval = -1;
    amxc_var_t node_path;
    qos_node_t n_queue;
    qos_node_t n_scheduler;

    amxc_var_init(&node_path);
    memset(&n_queue, 0, sizeof(n_queue));
    memset(&n_scheduler, 0, sizeof(n_scheduler));

    // No variant.
    retval = activate_queue(NULL, NULL, NULL);
    assert_int_equal(retval, -1);

    // Node path contains no cstring_t.
    retval = activate_queue(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Empty node path.
    variant_set_node_path(&node_path, "");
    retval = activate_queue(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Set path for remainder of the test.
    variant_set_node_path(&node_path, "QoS.Node.node-queue");

    // No node found.
    will_return(__wrap_qos_node_get_node, NULL);
    retval = activate_queue(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Node found, but invalid type.
    n_queue.type = QOS_NODE_TYPE_NONE;
    will_return(__wrap_qos_node_get_node, &n_queue);
    retval = activate_queue(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Node found, valid type.
    n_queue.type = QOS_NODE_TYPE_QUEUE;
    will_return(__wrap_qos_node_get_node, &n_queue);
    retval = activate_queue(NULL, &node_path, NULL);
    assert_int_equal(retval, 0);

    amxc_var_clean(&node_path);
}

void test_deactivate_queue(UNUSED void** state) {
    int retval = -1;
    amxc_var_t node_path;
    qos_node_t n_queue;

    amxc_var_init(&node_path);
    memset(&n_queue, 0, sizeof(n_queue));

    // No variant.
    retval = deactivate_queue(NULL, NULL, NULL);
    assert_int_equal(retval, -1);

    // Node path contains no cstring_t.
    retval = deactivate_queue(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Empty node path.
    variant_set_node_path(&node_path, "");
    retval = deactivate_queue(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Set path for remainder of the test.
    variant_set_node_path(&node_path, "QoS.Node.node-queue");

    // No node found.
    will_return(__wrap_qos_node_get_node, NULL);
    retval = deactivate_queue(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Node found, but invalid type.
    n_queue.type = QOS_NODE_TYPE_NONE;
    will_return(__wrap_qos_node_get_node, &n_queue);
    retval = deactivate_queue(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Node found, valid type.
    n_queue.type = QOS_NODE_TYPE_QUEUE;
    // - Set up libqosnode.
    will_return(__wrap_qos_node_get_node, &n_queue);
    retval = deactivate_queue(NULL, &node_path, NULL);
    assert_int_equal(retval, 0);

    amxc_var_clean(&node_path);
}

void test_activate_scheduler(UNUSED void** state) {
    int retval = -1;
    amxc_var_t node_path;
    qos_node_t n_scheduler;

    amxc_var_init(&node_path);
    memset(&n_scheduler, 0, sizeof(n_scheduler));

    // No variant.
    retval = activate_scheduler(NULL, NULL, NULL);
    assert_int_equal(retval, -1);

    // Node path contains no cstring_t.
    retval = activate_scheduler(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Empty node path.
    variant_set_node_path(&node_path, "");
    retval = activate_scheduler(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Set path for remainder of the test.
    variant_set_node_path(&node_path, "QoS.Node.node-scheduler");

    // No node found.
    will_return(__wrap_qos_node_get_node, NULL);
    retval = activate_scheduler(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Node found, but invalid type.
    n_scheduler.type = QOS_NODE_TYPE_NONE;
    will_return(__wrap_qos_node_get_node, &n_scheduler);
    retval = activate_scheduler(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Node found, valid type from here.
    n_scheduler.type = QOS_NODE_TYPE_SCHEDULER;

    // - Set up libqosnode.
    will_return(__wrap_qos_node_get_node, &n_scheduler);
    retval = activate_scheduler(NULL, &node_path, NULL);
    assert_int_equal(retval, 0);

    amxc_var_clean(&node_path);
}

void test_deactivate_scheduler(UNUSED void** state) {
    int retval = -1;
    amxc_var_t node_path;
    qos_node_t n_scheduler;

    amxc_var_init(&node_path);
    memset(&n_scheduler, 0, sizeof(n_scheduler));

    // No variant.
    retval = deactivate_scheduler(NULL, NULL, NULL);
    assert_int_equal(retval, -1);

    // Node path contains no cstring_t.
    retval = deactivate_scheduler(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Empty node path.
    variant_set_node_path(&node_path, "");
    retval = deactivate_scheduler(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Set path for remainder of the test.
    variant_set_node_path(&node_path, "QoS.Node.node-scheduler");

    // No node found.
    will_return(__wrap_qos_node_get_node, NULL);
    retval = deactivate_scheduler(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Node found, but invalid type.
    n_scheduler.type = QOS_NODE_TYPE_NONE;
    will_return(__wrap_qos_node_get_node, &n_scheduler);
    retval = deactivate_scheduler(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Node found, valid type from here.
    n_scheduler.type = QOS_NODE_TYPE_SCHEDULER;

    // - Set up libqosnode.
    will_return(__wrap_qos_node_get_node, &n_scheduler);
    retval = deactivate_scheduler(NULL, &node_path, NULL);
    assert_int_equal(retval, 0);

    amxc_var_clean(&node_path);
}

void test_activate_shaper(UNUSED void** state) {
    int retval = -1;
    amxc_var_t node_path;
    qos_node_t n_shaper;

    amxc_var_init(&node_path);
    memset(&n_shaper, 0, sizeof(n_shaper));

    // No variant.
    retval = activate_shaper(NULL, NULL, NULL);
    assert_int_equal(retval, -1);

    // Node path contains no cstring_t.
    retval = activate_shaper(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Empty node path.
    variant_set_node_path(&node_path, "");
    retval = activate_shaper(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Set path for remainder of the test.
    variant_set_node_path(&node_path, "QoS.Node.node-shaper");

    // No node found.
    will_return(__wrap_qos_node_get_node, NULL);
    retval = activate_shaper(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Node found, but invalid type.
    n_shaper.type = QOS_NODE_TYPE_NONE;
    will_return(__wrap_qos_node_get_node, &n_shaper);
    retval = activate_shaper(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Node found, valid type from here.
    n_shaper.type = QOS_NODE_TYPE_SHAPER;

    // - Set up libqosnode.
    will_return(__wrap_qos_node_get_node, &n_shaper);
    retval = activate_shaper(NULL, &node_path, NULL);
    assert_int_equal(retval, 0);

    amxc_var_clean(&node_path);
}

void test_deactivate_shaper(UNUSED void** state) {
    int retval = -1;
    amxc_var_t node_path;
    qos_node_t n_shaper;

    amxc_var_init(&node_path);
    memset(&n_shaper, 0, sizeof(n_shaper));

    // No variant.
    retval = deactivate_shaper(NULL, NULL, NULL);
    assert_int_equal(retval, -1);

    // Node path contains no cstring_t.
    retval = deactivate_shaper(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Empty node path.
    variant_set_node_path(&node_path, "");
    retval = deactivate_shaper(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Set path for remainder of the test.
    variant_set_node_path(&node_path, "QoS.Node.node-shaper");

    // No node found.
    will_return(__wrap_qos_node_get_node, NULL);
    retval = deactivate_shaper(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Node found, but invalid type.
    n_shaper.type = QOS_NODE_TYPE_NONE;
    will_return(__wrap_qos_node_get_node, &n_shaper);
    retval = deactivate_shaper(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Node found, valid type from here.
    n_shaper.type = QOS_NODE_TYPE_SHAPER;

    // - Set up libqosnode.
    will_return(__wrap_qos_node_get_node, &n_shaper);
    retval = deactivate_shaper(NULL, &node_path, NULL);
    assert_int_equal(retval, 0);

    amxc_var_clean(&node_path);
}

void test_retrieve_stats(UNUSED void** state) {
    int retval = -1;
    amxc_var_t node_path;
    amxc_var_t ret;
    qos_node_t n_queue;

    amxc_var_init(&node_path);
    amxc_var_init(&ret);
    memset(&n_queue, 0, sizeof(n_queue));

    // No variant.
    retval = retrieve_stats(NULL, NULL, NULL);
    assert_int_equal(retval, -1);

    // Node path contains no cstring_t.
    retval = retrieve_stats(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Empty node path.
    variant_set_node_path(&node_path, "");
    retval = retrieve_stats(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Set path for remainder of the test.
    variant_set_node_path(&node_path, "QoS.Node.node-queue");

    // No return variant.
    retval = retrieve_stats(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // No node found.
    will_return(__wrap_qos_node_get_node, NULL);
    retval = retrieve_stats(NULL, &node_path, &ret);
    assert_int_equal(retval, -1);

    // Node found, but invalid type.
    n_queue.type = QOS_NODE_TYPE_NONE;
    will_return(__wrap_qos_node_get_node, &n_queue);
    retval = retrieve_stats(NULL, &node_path, &ret);
    assert_int_equal(retval, -1);

    // Node found, valid type from here.
    n_queue.type = QOS_NODE_TYPE_QUEUE;

    // - Set up libqosnode.
    will_return(__wrap_qos_node_get_node, &n_queue);
    retval = retrieve_stats(NULL, &node_path, &ret);
    assert_int_equal(retval, 0);

    amxc_var_clean(&node_path);
    amxc_var_clean(&ret);
}

void test_retrieve_queue_stats(UNUSED void** state) {
    int retval = -1;
    amxc_var_t node_path;
    amxc_var_t ret;
    qos_node_t n_queue;

    amxc_var_init(&node_path);
    amxc_var_init(&ret);
    memset(&n_queue, 0, sizeof(n_queue));

    // No variant.
    retval = retrieve_stats(NULL, NULL, NULL);
    assert_int_equal(retval, -1);

    // Node path contains no cstring_t.
    retval = retrieve_stats(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Empty node path.
    variant_set_node_path(&node_path, "");
    retval = retrieve_stats(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // Set path for remainder of the test.
    variant_set_node_path(&node_path, "QoS.Node.node-queue");

    // No return variant.
    retval = retrieve_stats(NULL, &node_path, NULL);
    assert_int_equal(retval, -1);

    // No node found.
    will_return(__wrap_qos_node_get_node, NULL);
    retval = retrieve_stats(NULL, &node_path, &ret);
    assert_int_equal(retval, -1);

    // Node found, but invalid type.
    n_queue.type = QOS_NODE_TYPE_NONE;
    will_return(__wrap_qos_node_get_node, &n_queue);
    retval = retrieve_queue_stats(NULL, &node_path, &ret);
    assert_int_equal(retval, -1);

    // Node found, valid type from here.
    n_queue.type = QOS_NODE_TYPE_QUEUE;

    // - Set up libqosnode.
    will_return(__wrap_qos_node_get_node, &n_queue);
    retval = retrieve_queue_stats(NULL, &node_path, &ret);
    assert_int_equal(retval, 0);

    amxc_var_clean(&node_path);
    amxc_var_clean(&ret);
}

