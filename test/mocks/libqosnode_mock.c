/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone {
    return 0;
   }
   or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES {
    return 0;
   }
   LOSS OF
** USE, DATA, OR PROFITS {
    return 0;
   }
   OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include "libqosnode_mock.h"

#include <cmocka.h>

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

qos_node_t* __wrap_qos_node_get_node(UNUSED const char* path) {
    return mock_ptr_type(qos_node_t*);
}

qos_node_t* __wrap_qos_node_find_parent_by_type(const qos_node_t* const node, const qos_node_type_t type) {
    check_expected(node);
    check_expected(type);
    return mock_ptr_type(qos_node_t*);
}

qos_node_t* __wrap_qos_node_find_child_by_type(const qos_node_t* const node, const qos_node_type_t type) {
    check_expected(node);
    check_expected(type);
    return mock_ptr_type(qos_node_t*);
}

bool __wrap_qos_node_scheduler_get_enable(UNUSED const qos_node_t* node) {
    return mock_type(bool);
}

int32_t __wrap_qos_node_scheduler_get_shaping_rate(UNUSED const qos_node_t* node) {
    return mock_type(int32_t);
}

qos_scheduler_algorithm_t __wrap_qos_node_scheduler_get_scheduler_algorithm(UNUSED const qos_node_t* node) {
    return mock_type(qos_scheduler_algorithm_t);
}

int32_t __wrap_qos_node_queue_get_shaping_rate(UNUSED const qos_node_t* node) {
    return mock_type(int32_t);
}

int32_t __wrap_qos_node_queue_get_assured_rate(UNUSED const qos_node_t* node) {
    return mock_type(int32_t);
}

uint32_t __wrap_qos_node_scheduler_get_index(UNUSED const qos_node_t* node) {
    return mock_type(uint32_t);
}

uint32_t __wrap_qos_node_queue_get_weight(UNUSED const qos_node_t* node) {
    return mock_type(uint32_t);
}

uint32_t __wrap_qos_node_queue_get_precedence(UNUSED const qos_node_t* node) {
    return mock_type(uint32_t);
}

uint32_t __wrap_qos_node_queuestats_get_output_packets(UNUSED const qos_node_t* node) {
    return mock_type(uint32_t);
}

uint32_t __wrap_qos_node_queuestats_get_dropped_packets(UNUSED const qos_node_t* node) {
    return mock_type(uint32_t);
}

